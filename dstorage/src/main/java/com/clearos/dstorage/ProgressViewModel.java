package com.clearos.dstorage;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProgressViewModel extends ViewModel {
    private MutableLiveData<List<ProgressResult>> results;
    private Map<Integer, ProgressResult> resultCache;
    private Map<Integer, Long> blockStarts;
    private Map<Integer, Long> runTimes;
    private long started;

    private static final String TAG = "ProgressViewModel";

    public ProgressViewModel() {
        this.resultCache = new HashMap<>();
        blockStarts = new HashMap<>();
        runTimes = new HashMap<>();
        this.started = Instant.now().toEpochMilli();
    }

    /**
     * Sets the start time of an operation that this view is tracking progress for.
     */
    public void setStarted() {
        this.started = Instant.now().toEpochMilli();
    }

    /**
     * Returns a live data object for the progress results from a file upload.
     */
    public LiveData<List<ProgressResult>> getResults() {
        if (results == null) {
            results = new MutableLiveData<>();
        }
        return results;
    }

    public Long getStarted(int blockIndex) {
        return blockStarts.getOrDefault(blockIndex, 0L);
    }

    public Long getRuntime(int blockIndex) {
        return runTimes.getOrDefault(blockIndex, 0L);
    }

    public void appendResult(ProgressResult result) {
        Integer blockIndex = result.getBlockIndex();
        Long now = Instant.now().toEpochMilli();

        if (resultCache.containsKey(blockIndex)) {
            Long bStart = blockStarts.getOrDefault(blockIndex, started);
            assert bStart != null;
            runTimes.put(blockIndex, now - bStart);

            Log.d(TAG, String.format("Setting block start and runtime to %d and %d for index %d", bStart, now-bStart, blockIndex));
            result.setStarted(bStart);
            result.setRuntime(now - bStart);
        } else {
            Log.d(TAG, String.format("Setting block start to %d for index %d", now-started, blockIndex));
            blockStarts.put(blockIndex, now);
            result.setStarted(now-started);
        }

        resultCache.put(result.getBlockIndex(), result);
        List<ProgressResult> ordered = new ArrayList<>();
        for (int i=0; i < resultCache.size(); i++) ordered.add(resultCache.get(i));
        results.postValue(ordered);
    }
}
