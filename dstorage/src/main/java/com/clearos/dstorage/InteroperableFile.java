package com.clearos.dstorage;

import android.net.Uri;

import java.io.File;

/**
 * Represents a file that can be saved to decentralized storage from the local application's file
 * system.
 */
public class InteroperableFile {
    private File file;
    private Uri uri;
    private String label;
    private String fileName;

    public InteroperableFile(File file, Uri uri, String label, String fileName) {
        this.file = file;
        this.uri = uri;
        this.label = label;
        this.fileName = fileName;
    }

    public File getFile() {
        return file;
    }

    public Uri getUri() {
        return uri;
    }

    public String getFileName() {
        return fileName;
    }

    public String getLabel() {
        return label;
    }
}
