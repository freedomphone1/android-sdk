package com.clearos.dstorage;

public class RegistrationStatus {
    public static final int ENABLED = 2;
    public static final int UNREGISTERED = 0;
    public static final int DISABLED = 3;
    public static final int UNKNOWN = 4;
}
