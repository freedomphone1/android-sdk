package com.clearos.updates;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.clearos.dstorage.BuildConfig;
import com.clearos.updates.IAppUpdateCallback;
import com.clearos.updates.IAppUpdates;
import com.clearos.updates.IBetaAppUpdates;

import java.util.function.Function;

public class InAppUpdateClient {
    private static final String TAG = "InAppUpdateClient";
    private static final int CLEARLIFE_BETA_UPDATES_VERSION = 312;
    private static final String CLEARLIFE_PACKAGE_NAME = "com.clearos.clearlife";
    private static final String DIGITALLIFE_PACKAGE_NAME = "com.clearos.digitallife";

    private ServiceConnection updatesConnection;
    private IAppUpdates updateService;
    private IBetaAppUpdates betaUpdateService;
    private final Context appContext;
    private final IUpdateHandler handler;
    private final int timeout;
    private final boolean isBetaEnrolled;

    private Boolean supportsBetaUpdatesCheck = null;

    /**
     * Initialize a new client for performing in-app updates.
     * @param appContext Context of the application creating the client.
     * @param handler Handler for events created by the client.
     * @param timeout Number of milliseconds to wait for client to connect to service initially.
     * @param isBetaEnrolled When true, this client will request beta updates.
     */
    public InAppUpdateClient(Context appContext, IUpdateHandler handler, int timeout, boolean isBetaEnrolled) {
        this.appContext = appContext;
        this.handler = handler;
        this.timeout = timeout;
        this.isBetaEnrolled = isBetaEnrolled;
    }

    /**
     * Initialize a new client for performing in-app updates.
     * @param appContext Context of the application creating the client.
     * @param handler Handler for events created by the client.
     * @param timeout Number of milliseconds to wait for client to connect to service initially.
     */
    public InAppUpdateClient(Context appContext, IUpdateHandler handler, int timeout) {
        this(appContext, handler, timeout, false);
    }

    /**
     * Starts an in-app update within the ClearLIFE update service.
     * @param versionCode Version of the app to download and install.
     */
    public void startInAppUpdate(long versionCode) {
        try {
            if (isBetaEnrolled && supportsBetaUpdates())
                betaUpdateService.startAppUpdate(appContext.getPackageName(), versionCode);
            else
                updateService.startAppUpdate(appContext.getPackageName(), versionCode);
        } catch (RemoteException e) {
            Log.e(TAG, "Error calling update service method.", e);
            handler.onRemoteError(e);
        }
    }

    /**
     * Asks the update service to restart this app.
     * @param delaySeconds Number of seconds to wait before restart.
     */
    public void restartApp(int delaySeconds) {
        try {
            if (isBetaEnrolled && supportsBetaUpdates())
                betaUpdateService.restartApp(appContext.getPackageName(), delaySeconds);
            else
                updateService.restartApp(appContext.getPackageName(), delaySeconds);
        } catch (RemoteException e) {
            Log.e(TAG, "Error calling app restart method.", e);
            handler.onRemoteError(e);
        }
    }

    /**
     * Checks whether the installed version of ClearLIFE on the user's phone supports the beta
     * channel for in-app updates.
     * @return False if the version number is too low or if the version code check failed.
     */
    public boolean supportsBetaUpdates() {
        // Do some simple in-memory caching so we don't keep looking up package version numbers.
        if (supportsBetaUpdatesCheck == null)
            supportsBetaUpdatesCheck = latestClearLifeVersion() >= CLEARLIFE_BETA_UPDATES_VERSION;
        return supportsBetaUpdatesCheck;
    }

    /**
     * Gets the version number of the latest ClearLIFE version, which allows checking compatibility
     * with ClearLIFE features.
     */
    public int latestClearLifeVersion() {
        try {
            PackageInfo packageInfo;
            if (Build.MODEL.contains("ClearPHONE") || Build.MODEL.contains("FreedomPhone"))
                packageInfo = appContext.getPackageManager().getPackageInfo(CLEARLIFE_PACKAGE_NAME, PackageManager.GET_META_DATA);
            else
                packageInfo = appContext.getPackageManager().getPackageInfo(DIGITALLIFE_PACKAGE_NAME, PackageManager.GET_META_DATA);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package version information. Aborting package update availability check.", e);
            return 0;
        }
    }

    /**
     * Gets the latest version of the calling package available in the app store.
     */
    @RequiresApi(api = Build.VERSION_CODES.P)
    public void getLatestVersion() {
        PackageInfo packageInfo;
        try {
            packageInfo = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package version information. Aborting package update availability check.", e);
            return;
        }

        try {
            if (isBetaEnrolled && supportsBetaUpdates())
                betaUpdateService.getUpdateInfo(packageInfo.packageName, packageInfo.getLongVersionCode());
            else
                updateService.getUpdateInfo(packageInfo.packageName, packageInfo.getLongVersionCode());
        } catch (RemoteException e) {
            Log.e(TAG, "Error calling update service method.", e);
            handler.onRemoteError(e);
        }
    }

    private void assertPackageNameMatch(String packageName) {
        if (BuildConfig.DEBUG && !packageName.equals(appContext.getPackageName())) {
            throw new AssertionError("Callback fired for the wrong package. Inconceivable!");
        }
    }

    private final IAppUpdateCallback mCallback = new IAppUpdateCallback.Stub() {
        @Override
        public void onLatestVersion(String packageName, UpdateInfo info) {
            assertPackageNameMatch(packageName);
            handler.onUpdateCheck(info);
        }

        @Override
        public void onDownloadProgress(String packageName, float progress) {
            assertPackageNameMatch(packageName);
            handler.onDownloadProgress(progress);
        }

        @Override
        public void onInstallStarted(String packageName) {
            assertPackageNameMatch(packageName);
            handler.onInstallStarted();
        }

        @Override
        public void onInstallCompleted(String packageName, String version) {
            assertPackageNameMatch(packageName);
            handler.onInstallCompleted(version);
        }

        @Override
        public void onInstallFailed(String packageName, String errorMessage) {
            assertPackageNameMatch(packageName);
            handler.onInstallFailed(errorMessage);
        }
    };


    public IAppUpdates create(Context appContext, Function<InAppUpdateClient, Void> callback) {
        Log.d(TAG, "Creating service connection to ClearLIFE app updates.");
        updatesConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected; wiring up service interface.");
                updateService = IAppUpdates.Stub.asInterface(service);
                Log.d(TAG, "Binding - In-App Updates Service connected");

                // Finally register a callback interface so we can receive notifications about
                // completed update operations
                try {
                    updateService.registerCallback(mCallback);
                    Log.d(TAG, "Callback registered with in-app update service.");
                } catch (RemoteException e) {
                    Log.e(TAG, "Error registering the callback functions for client.", e);
                }

                if (callback != null) {
                    callback.apply(InAppUpdateClient.this);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                updateService = null;
                Log.d(TAG, "Binding - In-App Updates Service disconnected");
            }

            @Override
            public void onBindingDied(ComponentName name) {
                Log.d(TAG, "Binding died for " + name.flattenToString());
            }

            @Override
            public void onNullBinding(ComponentName name) {
                Log.d(TAG, "Null binding for " + name.flattenToString());
            }
        };

        if (updateService == null) {
            Intent it = new Intent("AppUpdatesService");
            if (Build.MODEL.contains("ClearPHONE") || Build.MODEL.contains("FreedomPhone")) {
                it.setPackage("com.clearos.clearlife");
                it.setAction("ClearLIFE.InAppUpdates");
            } else {
                it.setPackage("com.clearos.digitallife");
                it.setAction("DigitalLife.InAppUpdates");
            }
            Log.d(TAG, "Calling bindService with inAppUpdates connection.");
            if (!appContext.bindService(it, updatesConnection, Service.BIND_AUTO_CREATE)) {
                Log.e(TAG, "Bind service call was not successful.");
                callback.apply(null);
            } else {
                new Handler(appContext.getMainLooper()).postDelayed(() -> {
                    if (updateService == null) {
                        Log.w(TAG, "Update client binding not established within timeout period.");
                        callback.apply(null);
                    }
                }, timeout);
            }
        }
        return updateService;
    }

    public IBetaAppUpdates createBeta(Context appContext, Function<InAppUpdateClient, Void> callback) {
        if (!supportsBetaUpdates()) {
            Log.w(TAG, "ClearLIFE version doesn't support beta updates; returning null.");
            return null;
        }

        Log.d(TAG, "Creating service connection to ClearLIFE beta app updates.");
        updatesConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected; wiring up service interface.");
                betaUpdateService = IBetaAppUpdates.Stub.asInterface(service);
                Log.d(TAG, "Binding - Beta In-App Updates Service connected");

                // Finally register a callback interface so we can receive notifications about
                // completed update operations
                try {
                    betaUpdateService.registerCallback(mCallback);
                    Log.d(TAG, "Callback registered with in-app update service.");
                } catch (RemoteException e) {
                    Log.e(TAG, "Error registering the callback functions for client.", e);
                }

                if (callback != null) {
                    callback.apply(InAppUpdateClient.this);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                betaUpdateService = null;
                Log.d(TAG, "Binding - In-App Updates Service disconnected");
            }

            @Override
            public void onBindingDied(ComponentName name) {
                Log.d(TAG, "Binding died for " + name.flattenToString());
            }

            @Override
            public void onNullBinding(ComponentName name) {
                Log.d(TAG, "Null binding for " + name.flattenToString());
            }
        };

        if (betaUpdateService == null) {
            Intent it = new Intent("BetaAppUpdatesService");
            if (Build.MODEL.contains("ClearPHONE") || Build.MODEL.contains("FreedomPhone")) {
                it.setPackage("com.clearos.clearlife");
                it.setAction("ClearLIFE.InAppBetaUpdates");
            } else {
                it.setPackage("com.clearos.digitallife");
                it.setAction("DigitalLife.InAppBetaUpdates");
            }
            Log.d(TAG, "Calling bindService with inAppBetaUpdates connection.");
            if (!appContext.bindService(it, updatesConnection, Service.BIND_AUTO_CREATE)) {
                Log.e(TAG, "Bind service call was not successful.");
                callback.apply(null);
            } else {
                new Handler(appContext.getMainLooper()).postDelayed(() -> {
                    if (betaUpdateService == null) {
                        Log.w(TAG, "Update client binding not established within timeout period.");
                        callback.apply(null);
                    }
                }, timeout);
            }
        }
        return betaUpdateService;
    }

    /**
     * Unbinds the crypto derived keys service from this app.
     */
    public void destroy() {
        // Unregister the callback so that the server doesn't try to send stuff that we won't receive.
        Log.d(TAG, "Cleaning up the In-App update client.");
        if (updateService != null) {
            try {
                updateService.uregisterCallback(mCallback);
            } catch (RemoteException e) {
                Log.e(TAG, "Error unregistering the callback functions for client.", e);
            }
        }

        if (betaUpdateService != null) {
            try {
                betaUpdateService.uregisterCallback(mCallback);
            } catch (RemoteException e) {
                Log.e(TAG, "Error unregistering the callback functions for client.", e);
            }
        }
        appContext.unbindService(updatesConnection);
    }
}
