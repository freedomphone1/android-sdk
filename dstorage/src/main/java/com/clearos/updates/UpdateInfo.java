package com.clearos.updates;

import android.os.Parcel;
import android.os.Parcelable;

public class UpdateInfo implements Parcelable {
    public transient static final int DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS = 3; // 0x3
    public transient static final int UNKNOWN = 0; // 0x0
    public transient static final int UPDATE_AVAILABLE = 2; // 0x2
    public transient static final int UPDATE_NOT_AVAILABLE = 1; // 0x1
    public transient static final int NETWORK_NOT_AVAILABLE = 4;

    private long versionCode;
    private String versionName;
    private String packageName;
    private int availability;
    private long fileSize;
    private int importance;

    public UpdateInfo(String packageName, String versionName, long versionCode, int availability, long fileSize) {
        this.packageName = packageName;
        this.versionName = versionName;
        this.versionCode = versionCode;
        this.availability = availability;
        this.fileSize = fileSize;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }
    public int getAvailability() {
        return availability;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public long getFileSize() {
        return fileSize;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public String getPackageName() {
        return packageName;
    }

    public void setVersionCode(long versionCode) {
        this.versionCode = versionCode;
    }
    public long getVersionCode() {
        return versionCode;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
    public String getVersionName() {
        return versionName;
    }

    public void setImportance(int importance) {
        this.importance = importance;
    }
    public int getImportance() {
        return importance;
    }

    public UpdateInfo(Parcel in) {
        versionCode = in.readLong();
        versionName = in.readString();
        packageName = in.readString();
        availability = in.readInt();
        fileSize = in.readLong();
        importance = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(versionCode);
        dest.writeString(versionName);
        dest.writeString(packageName);
        dest.writeInt(availability);
        dest.writeLong(fileSize);
        dest.writeInt(importance);
    }

    public static final Parcelable.Creator<UpdateInfo> CREATOR
            = new Parcelable.Creator<UpdateInfo>() {
        public UpdateInfo createFromParcel(Parcel in) {
            return new UpdateInfo(in);
        }

        public UpdateInfo[] newArray(int size) {
            return new UpdateInfo[size];
        }
    };
}
