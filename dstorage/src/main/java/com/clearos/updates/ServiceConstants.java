package com.clearos.updates;

public class ServiceConstants {
    public final static String RESTART_REASON_EXTRA = "clearosRestartReason";
    public final static String RESTART_REASON_INSTALL_COMPLETED = "installCompleted";
    public final static String RESTART_REASON_APP_REQUESTED = "appRequestedRestart";
}
