package com.clearos.updates;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.clearos.dstorage.R;
import com.clearos.identity.KeyClientApplication;
import com.clearos.identity.SharedPrefs;
import com.google.android.material.snackbar.Snackbar;

import java.time.Instant;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.clearos.updates.ServiceConstants.RESTART_REASON_EXTRA;
import static com.clearos.updates.ServiceConstants.RESTART_REASON_INSTALL_COMPLETED;


@SuppressLint("Registered")
public class InAppUpdateActivity extends AppCompatActivity implements IUpdateHandler {
    private final String TAG = "InAppUpdater";
    private Function<Boolean, Void> updateCallback;
    public static final String RESET_UPDATE_INTERVAL_EXTRA = "resetUpdateInterval";
    public static final String INTENT_PACKAGE_NAME = "INTENT_PACKAGE_NAME";
    public static final String STRING_REPO = "STRING_REPO";
    public static final String CLEAR_APPS_PACKAGE_NAME = "com.clearos.marketplace";

    private CountDownLatch updateLatch = null;

    private UpdateInfo info;
    private InAppUpdateClient updateClient;
    private ProgressBar progressBar;
    private TextView statusLabel;

    private boolean updateCompleted = false;
    private boolean forceUpdate = false;
    private boolean isBetaEnrolled = false;
    private int forceUpdateImportance = 100;
    private Runnable updateSegue;
    private long updateCheckInterval = TimeUnit.DAYS.toSeconds(10);

    private String[] requiredPermissions;
    private final static int REQUEST_REQUIRED_PERMISSIONS = 136;
    private boolean appRequiresInternet = false;

    private final static Object syncBusy = new Object();
    private static boolean resumeBusy = false;
    private boolean noInternetHold = false;

    private Network capableNetwork;
    private final CountDownLatch networkAvailableLatch = new CountDownLatch(1);

    private boolean updateViaWebOrMarketplace = false;

    /**
     * Sets the permissions that the app should request from the user on startup.
     */
    public void setRequiredPermissions(String[] requiredPermissions) {
        this.requiredPermissions = requiredPermissions;
    }

    /**
     * Configures the update checker to use the beta update source for in-app updates.
     * @param isBetaEnrolled True if the app should do in-app updates from beta stream.
     */
    public void setBetaEnrolled(boolean isBetaEnrolled) {
        this.isBetaEnrolled = isBetaEnrolled;
    }

    /**
     * Sets the function that should run once the update check has completed.
     */
    public void setUpdateSegue(Runnable r) {
        this.updateSegue = r;
    }

    /**
     * Sets the number of seconds that should elapse between update checks.
     */
    public void setUpdateCheckInterval(long seconds) {
        this.updateCheckInterval = seconds;
    }

    /**
     * Sets the UI elements to use for displaying in-app update progress.
     */
    public void setUpdateUiElements(ProgressBar progressBar, TextView statusLabel) {
        this.progressBar = progressBar;
        this.statusLabel = statusLabel;
    }

    /**
     * Check if there is a no-internet hold on the application startup. This is only the case
     * if the app has set `appRequiresInternet`.
     * @return True if no internet is available.
     */
    public boolean isNoInternetHold() {
        return noInternetHold;
    }

    /**
     * Tell the update activity that this app requires an internet connection to run.
     * @param appRequiresInternet True if it shouldn't start without internet.
     */
    public void setAppRequiresInternet(boolean appRequiresInternet) {
        this.appRequiresInternet = appRequiresInternet;
        if (appRequiresInternet && capableNetwork == null)
            registerNetworkMonitor();
    }

    private void doUpdateCheck() {
        updateClient = new InAppUpdateClient(getApplicationContext(), this, 2500, isBetaEnrolled);
        if (isBetaEnrolled && updateClient.supportsBetaUpdates())
            updateClient.createBeta(getApplicationContext(), this::clientCreatorCallback);
        else
            // Fall back to the vanilla update check system.
            updateClient.create(getApplicationContext(), this::clientCreatorCallback);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private Void clientCreatorCallback(InAppUpdateClient client) {
        if (client != null)
            client.getLatestVersion();
        else
            updateCallback.apply(false);
        return null;
    }

    /**
     * Returns an intent that will trigger an update check.
     * @param from Activity to transition from.
     * @param splash "Splash" activity that has the update logic.
     */
    public static Intent getCheckForUpdatesIntent(Activity from, Class<?> splash) {
        Intent result = new Intent(from, splash);
        result.putExtra(RESET_UPDATE_INTERVAL_EXTRA, true);
        return result;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String reason = getIntent().getStringExtra(RESTART_REASON_EXTRA);
        if (reason != null && reason.equals(RESTART_REASON_INSTALL_COMPLETED))
            updateCompleted = true;

        boolean shouldResetInterval = getIntent().getBooleanExtra(RESET_UPDATE_INTERVAL_EXTRA, false);
        if (shouldResetInterval) {
            Log.i(TAG, "Got explicit interval reset extra; checking for updates.");
            resetUpdateInterval(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (appRequiresInternet && !hasCapableNetwork()) {
            Log.d(TAG, "No internet detected. Showing snackbar message.");
            popupSnackbarForNoNetwork();
            noInternetHold = true;
            return;
        }

        if (updateViaWebOrMarketplace && !forceUpdate)
            updateCallback.apply(true);

        Log.d(TAG, "Resuming activity and checking for updates in progress.");
        if (updateLatch != null) {
            Log.d(TAG, "Update latch is not null; an update check has started before...");
            if (updateClient == null) {
                Log.d(TAG, "Update client doesn't exist; exiting resume in updater.");
                return;
            }

            try {
                Log.d(TAG, "Waiting for update latch to complete.");
                updateLatch.await(2, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Log.w(TAG, "Interrupted thread waiting for existing update info call to finish.");
                return;
            }

            if (updateLatch.getCount() == 0) {
                Log.d(TAG, "Progressing to check server for in-app update.");
                checkInAppUpdate(updateCallback);
                return;
            }
        }

        synchronized (syncBusy) {
            Log.d(TAG, String.format("Update check resume busy? %b; no internet hold? %b", resumeBusy, noInternetHold));
            if (!resumeBusy && !noInternetHold) {
                resumeBusy = true;
                // Check permissions for the app to access the derived key service.
                if (requiredPermissions != null && requiredPermissions.length > 0) {
                    boolean anyDenials = Stream.of(requiredPermissions).anyMatch(p -> checkCallingOrSelfPermission(p) == PackageManager.PERMISSION_DENIED);
                    if (anyDenials) {
                        boolean anyRationaleRequired = Stream.of(requiredPermissions).anyMatch(this::shouldShowRequestPermissionRationale);
                        if (anyRationaleRequired) {
                            Log.d(TAG, "Need to show rationale...");
                        } else {
                            if (KeyClientApplication.getInstance().isClearLifeInstalled())
                                requestPermissions(requiredPermissions, REQUEST_REQUIRED_PERMISSIONS);
                            else {
                                Log.w(TAG, "ClearLIFE is not installed on the phone. No sense in continuing.");
                                updateSegue.run();
                            }
                        }
                    } else
                        runUpdateCheck();
                } else
                    runUpdateCheck();
            }
        }
    }

    /**
     * Sets the value of the resume busy synchronization flag.
     * @param busy True if a previous `onResume` method is still running.
     */
    public void setResumeBusy(boolean busy) {
        synchronized (syncBusy) {
            resumeBusy = busy;
        }
    }

    private void runUpdateCheck() {
        checkInAppUpdate(critical -> {
            Log.d(TAG, "In-app update callback; segue into next activity.");
            updateSegue.run();
            return null;
        }, updateCheckInterval, TimeUnit.SECONDS);

        new Handler(getMainLooper()).postDelayed(() -> {
             if (updateLatch != null && updateLatch.getCount() == 1) {
                 Log.d(TAG, "No response after 15 seconds; segue anyway.");
                 updateSegue.run();
             }
        }, 15000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        synchronized (syncBusy) {
            resumeBusy = false;
        }

        if (requestCode == REQUEST_REQUIRED_PERMISSIONS) {
            Log.d(TAG, String.format("Permission request result for %s is %s.",
                    String.join(", ", permissions),
                    Stream.of(grantResults)
                            .map(Object::toString)
                            .collect(Collectors.joining(",","[","]"))));
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runUpdateCheck();
            } else {
                Toast.makeText(getApplicationContext(), "No permission to access derived keys.", Toast.LENGTH_LONG).show();
                updateSegue.run();
            }
        }
    }

    private void registerNetworkMonitor() {
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkRequest request = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .build();
            ConnectivityManager.NetworkCallback callback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    if (capableNetwork.equals(network))
                        capableNetwork = null;
                }

                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    networkAvailableLatch.countDown();
                    capableNetwork = network;
                }

                @Override
                public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties linkProperties) {
                    super.onLinkPropertiesChanged(network, linkProperties);
                    capableNetwork = network;
                }
            };
            cm.requestNetwork(request, callback);
        }
    }

    public boolean hasCapableNetwork() {
        if (capableNetwork == null) {
            try {
                Log.d(TAG, "Waiting for a ready network...");
                networkAvailableLatch.await(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Log.d(TAG, "Network ready latch interrupted while waiting.");
            }
        }
        return capableNetwork != null;
    }

    /**
     * Sets the importance level at which an update becomes critical and the app will not proceed
     * without being updated.
     * @param forceUpdateImportance Level specified in the `latest/version` file of the app-store.
     */
    public void setForceUpdateImportance(int forceUpdateImportance) {
        this.forceUpdateImportance = forceUpdateImportance;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public UpdateInfo getUpdateInfo() {
        return info;
    }

    private void resumeUpdate(UpdateInfo info) {
        Log.d(TAG, "Starting update flow for resumed update.");
        updateClient.startInAppUpdate(info.getVersionCode());
    }

    private void startUpdate(UpdateInfo info) {
        Log.d(TAG, "Found update; starting flow.");

        if (Utils.isClearDevice())
            updateClient.startInAppUpdate(info.getVersionCode());
        else {
            if (KeyClientApplication.getInstance().isPackageInstalled(CLEAR_APPS_PACKAGE_NAME))
                popupSnackbarForUpdateOnMarketplace(info);
            else
                popupSnackbarForUpdateOnWeb(info);
        }
    }

    /**
     * Returns the name of the key to use in shared preferences for the last update check time.
     * @param appContext Context of the calling application that sub-classed this activity.
     */
    private static String packageUpdatePrefsKey(Context appContext) {
        return String.format("%s/updates/checked", appContext.getPackageName());
    }

    /**
     * Deletes the time when updates were last checked so that the next request to check updates
     * will definitely fire.
     */
    public static void resetUpdateInterval(Context appContext) {
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);
        prefs.rmValue(packageUpdatePrefsKey(appContext));
    }


    /**
     * Checks to see if the interval between update checks has elapsed.
     * @param frequency How long to wait between update checks.
     * @param unit Time unit to apply to frequency.
     * @return `true` if the interval has elapsed; false otherwise.
     */
    public boolean hasIntervalElapsed(long frequency, TimeUnit unit) {
        Log.d(TAG, "Checking if update interval has elapsed");
        Context appContext = getApplicationContext();
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);

        // This method returns 0 if the value doesn't exist. So the difference will be way bigger
        // than any configured interval.
        long lastCheck = prefs.getLong(packageUpdatePrefsKey(appContext));
        long now = Instant.now().toEpochMilli();
        long freqMillis = unit.toMillis(frequency);
        Log.d(TAG, String.format("Last checked for updates at %d; now is %d; frequency is %d", lastCheck, now, freqMillis));
        return (now-lastCheck) > freqMillis;
    }

    /**
     * Triggers a check for updates from upstream server if it hasn't been done in the last 24 hours.
     * @param callback Function to call after the check is completed. In parameter is true if a
     *                 critical update is available; otherwise false.
     */
    public void checkInAppUpdate(Function<Boolean, Void> callback) {
        checkInAppUpdate(callback, 10, TimeUnit.DAYS);
    }

    /**
     * Triggers a check for updates from upstream server.
     * @param callback Function to call after the check is completed. In parameter is true if a
     *                 critical update is available; otherwise false.
     * @param frequency How long to wait between update checks.
     * @param unit Time unit to apply to frequency.
     */
    public void checkInAppUpdate(Function<Boolean, Void> callback,
                                 long frequency, TimeUnit unit) {
        if (updateCompleted) {
            updateCallback = callback;
            onInstallCompleted("latest");
        } else {
            new Handler().postDelayed(() -> triggerInAppUpdate(callback, frequency, unit), 150);
        }
    }

    private void triggerInAppUpdate(Function<Boolean, Void> callback,
                                    long frequency, TimeUnit unit) {
        // Update the status label to reflect the in-app update status.
        statusLabel.setText(R.string.updatesChecking);
        statusLabel.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);

        if (forceUpdate || hasIntervalElapsed(frequency, unit)) {
            Log.d(TAG, "Checking for in-app updates; interval has elapsed.");
            updateCallback = (Boolean critical) -> {
                Log.d(TAG, "Update callback fired; storing update check time.");
                SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
                long now = Instant.now().toEpochMilli();
                prefs.saveLong(packageUpdatePrefsKey(getApplicationContext()), now);
                callback.apply(critical);
                return null;
            };

            updateLatch = new CountDownLatch(1);
            new Handler().postDelayed(this::doUpdateCheck, 150);
        } else {
            Log.d(TAG, "Interval for update checks has not yet elapsed.");
            new Handler(getMainLooper()).post(() -> {
                statusLabel.setText(getString(R.string.updatesNotElapsed));
                progressBar.setVisibility(View.INVISIBLE);
            });
            callback.apply(null);
        }
    }

    private void restartApp(View view) {
        Log.d(TAG, "Triggering restart app for update.");
        if (updateClient != null) {
            updateClient.restartApp(1);
            finishAffinity();
        }
    }

    private void updateOnWeb(UpdateInfo info) {
        Log.d(TAG, "Triggering update on web for update.");
        updateViaWebOrMarketplace = true;
        Intent intent = new Intent()
                .setAction(Intent.ACTION_VIEW)
                .setData(Uri.parse(String.format("https://clearos.app/app/download/%s", info.getPackageName())));
        startActivity(intent);
    }

    private void updateOnMarketplace(UpdateInfo info) {
        Log.d(TAG, "Triggering update on marketplace for update.");
        updateViaWebOrMarketplace = true;
        Intent intent = new Intent()
                .setAction(Intent.ACTION_VIEW)
                .setClassName(CLEAR_APPS_PACKAGE_NAME, String.format("%s.ui.details.DetailsActivity", CLEAR_APPS_PACKAGE_NAME))
                .putExtra(INTENT_PACKAGE_NAME, info.getPackageName())
                .putExtra(STRING_REPO, "ClearAPPS");
        startActivity(intent);
    }

    private void popupSnackbarForRestartApp() {
        String message;
        if (forceUpdate)
            message = getString(R.string.forcedUpdateFailed);
        else
            message = getString(R.string.unforcedUpdateFailed);

        Snackbar snackbar = Snackbar.make(
                        findViewById(android.R.id.content),
                        message,
                        Snackbar.LENGTH_INDEFINITE);

        if (!forceUpdate)
            snackbar.setAction("OK", v -> updateCallback.apply(true));
        else
            snackbar.setAction("OK", this::restartApp);

        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }

    @Override
    public void onUpdateCheck(UpdateInfo info) {
        Log.i(TAG, String.format("Received update info: %d with availability %d", info.getVersionCode(), info.getAvailability()));
        updateLatch.countDown();
        String statusUpdate = "Unknown status";
        boolean forceUpdateMessage = false;

        switch (info.getAvailability()) {
            case UpdateInfo.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS:
                Log.d(TAG, "An update is in progress; resuming update.");
                resumeUpdate(info);
                statusUpdate = getString(R.string.updatesInstalling);
                break;
            case UpdateInfo.UPDATE_AVAILABLE:
                Log.d(TAG, "A new update is available; starting update.");
                statusUpdate = getString(R.string.updatesFound);
                startUpdate(info);
                break;
            case UpdateInfo.UPDATE_NOT_AVAILABLE:
                Log.d(TAG, "No critical updates were found; running callback.");
                setProgressBarInvisible();
                statusUpdate = getString(R.string.updatesNoUpdateAvailable);
                updateCallback.apply(false);
                break;
            case UpdateInfo.UNKNOWN:
                statusUpdate = getString(R.string.updatesStatusUnknown);
                setProgressBarInvisible();
                if (forceUpdate || info.getImportance() >= forceUpdateImportance) {
                    setForceUpdate(true);
                    forceUpdateMessage = true;
                } else
                    updateCallback.apply(false);
                break;
            case UpdateInfo.NETWORK_NOT_AVAILABLE:
                statusUpdate = getString(R.string.updatesNoNetwork);
                setProgressBarInvisible();
                if (forceUpdate || info.getImportance() >= forceUpdateImportance) {
                    setForceUpdate(true);
                    forceUpdateMessage = true;
                } else
                    updateCallback.apply(false);
                break;
        }
        this.info = info;
        String finalStatusUpdate = statusUpdate;
        boolean finalForceUpdateMessage = forceUpdateMessage;
        new Handler(getMainLooper()).post(() -> {
            statusLabel.setText(finalStatusUpdate);
            if (finalForceUpdateMessage)
                popupSnackbarForUpdateForce();
        });
    }

    private void setProgressBarInvisible() {
        new Handler(getMainLooper()).post(() -> progressBar.setVisibility(View.INVISIBLE));
    }

    @Override
    public void onDownloadProgress(float progress) {
        new Handler(getMainLooper()).post(() -> {
            statusLabel.setText(String.format(getString(R.string.updatesDownloading), progress*100));
            progressBar.setProgress((int) Math.ceil(progress*100));
            progressBar.setIndeterminate(false);
            progressBar.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void onInstallStarted() {
        new Handler(getMainLooper()).post(() -> {
            statusLabel.setText(R.string.updatesInstalling);
            progressBar.setProgress(0);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
        });
    }

    private void popupSnackbarForUpdateOnWeb(UpdateInfo info) {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.updateOnWebSnackbar),
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Install Now", (view) -> updateOnWeb(info));
        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private void popupSnackbarForUpdateOnMarketplace(UpdateInfo info) {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.updateOnMarketplaceSnackbar),
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", (view) -> updateOnMarketplace(info));
        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private void popupSnackbarForUpdateForce() {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.updateForceSnackbar),
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", this::restartApp);
        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private void popupSnackbarForNoNetwork() {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.noInternetMessage),
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", v -> finishAffinity());
        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }


    private void popupSnackbarForUpdateComplete() {
        Snackbar snackbar = Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.updateCompletedSnackbar),
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", v -> updateCallback.apply(false));
        snackbar.setActionTextColor(
                getColor(R.color.colorPrimary));
        snackbar.show();
    }

    @Override
    public void onInstallCompleted(String version) {
        Log.i(TAG, "Finished install update; app is now at version " + version);
        new Handler(getMainLooper()).post(() -> {
            statusLabel.setText(R.string.updatesCompleted);
            progressBar.setIndeterminate(false);
            progressBar.setProgress(100);
        });
        popupSnackbarForUpdateComplete();
    }

    @Override
    public void onInstallFailed(String errorMessage) {
        Log.e(TAG, "Failed getting update status from ClearLIFE: " + errorMessage);
        new Handler(getMainLooper()).post(() -> {
            statusLabel.setText(R.string.updatesFailed);
            progressBar.setProgress(0);
            progressBar.setIndeterminate(false);
            progressBar.setVisibility(View.VISIBLE);
        });
        if (Utils.isClearDevice())
            popupSnackbarForRestartApp();
        else {
            if (KeyClientApplication.getInstance().isPackageInstalled(CLEAR_APPS_PACKAGE_NAME))
                popupSnackbarForUpdateOnMarketplace(info);
            else
                popupSnackbarForUpdateOnWeb(info);
        }
    }

    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error querying update status with ClearLIFE: " + e.getMessage());
        if (!forceUpdate)
            updateCallback.apply(null);
        else
            popupSnackbarForUpdateForce();
    }

    @Override
    protected void onStop() {
        super.onStop();
        setResumeBusy(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateClient != null)
            updateClient.destroy();
    }
}
