// IAppUpdateCallback.aidl
package com.clearos.updates;

import com.clearos.updates.UpdateInfo;

oneway interface IAppUpdateCallback {
    /**
     * Fires when the latest version of a package has been retrieved.
     */
    oneway void onLatestVersion(String packageName, in UpdateInfo info);
    /**
     * Fires on block download progress for an app from decentralized storage.
     */
    oneway void onDownloadProgress(String packageName, float progress);
    /**
     * Fires once download is completed and installation has started.
     */
    oneway void onInstallStarted(String packageName);
    /**
     * Fires when the download of the new package APK is completed.
     */
    oneway void onInstallCompleted(String packageName, String version);
    /**
     * Fires if the installation fails.
     */
    oneway void onInstallFailed(String packageName, String errorMessage);
}
