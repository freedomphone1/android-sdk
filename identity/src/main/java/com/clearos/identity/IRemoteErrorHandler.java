package com.clearos.identity;

import android.os.RemoteException;

import com.clearos.dlt.DecryptedMqttMessage;

import java.io.IOException;

public interface IRemoteErrorHandler {
    void onRemoteError(RemoteException e);
    void onGenericError(Exception e);
    void onIOError(IOException e);
    void onNotificationReceived(DecryptedMqttMessage message);
}
