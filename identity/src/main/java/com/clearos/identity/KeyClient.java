package com.clearos.identity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DidKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class KeyClient implements IRemoteErrorHandler {
    private final String TAG = "KeyClient";

    private static final Map<String, DerivedKeyClient> packageKeyClients = new ConcurrentHashMap<>();
    private final Object syncSetup = new Object();
    private Map<String, List<Function<KeyClient, Void>>> registrationCallbacks;
    private static final Map<String, Function<DecryptedMqttMessage, Void>> packageNotificationCallbacks = new ConcurrentHashMap<>();
    private static final Map<String, Function<DidKeys, Void>> packageKeyCallbacks = new ConcurrentHashMap<>();
    private static final Map<String, Boolean> packageKeyClientConnected = new ConcurrentHashMap<>();

    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error from derived key client children.", e);
    }
    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived key client from document provider.", e);
    }
    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "I/O error in decentralized storage from document provider.", e);
    }
    @Override
    public void onNotificationReceived(DecryptedMqttMessage message) {
        Function<DecryptedMqttMessage, Void> notificationCallback = packageNotificationCallbacks.getOrDefault(message.getPackageName(), null);
        if (notificationCallback != null) {
            notificationCallback.apply(message);
        }
        Log.i(TAG, "Notification message received: " + message);
    }

    /**
     * Removes all registration callbacks from the application instance.
     */
    public void clearRegistrationCallbacks(String packageName) {
        registrationCallbacks.remove(packageName);
    }

    /**
     * Adds a function to call once the app has successfully registered.
     */
    public void addRegistrationCallback(String packageName, Function<KeyClient, Void> callback) {
        if (!registrationCallbacks.containsKey(packageName))
            registrationCallbacks.put(packageName, new ArrayList<>());
        List<Function<KeyClient, Void>> callbacks = registrationCallbacks.get(packageName);
        if (callbacks != null)
            callbacks.add(callback);
    }

    public void create() {
        registrationCallbacks = new ConcurrentHashMap<>();
    }

    public void terminate() {
        for (Map.Entry<String,DerivedKeyClient> entry: packageKeyClients.entrySet()) {
            DerivedKeyClient keyClient = entry.getValue();
            if (keyClient != null) {
                keyClient.destroy();
            }
        }
    }

    public DidKeys getAppKeys(String packageName) {
        DerivedKeyClient packageClient = packageKeyClients.getOrDefault(packageName, null);
        if (packageClient != null)
            return packageClient.getAppKeys();
        else
            return null;
    }

    public DidKeys getAppKeys(Context context) {
        return getAppKeys(context.getPackageName());
    }

    /**
     * Checks whether the package with specified name is installed.
     * @param packageName FQN of the package to check.
     * @return True if the package is installed.
     */
    public boolean isPackageInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Check is ClearLIFE is installed.
     * @return True if ClearLIFE is installed; otherwise false.
     */
    public boolean isClearLifeInstalled(Context context) {
        if (Build.MODEL.contains("ClearPHONE") || Build.MODEL.contains("FreedomPhone"))
            return isPackageInstalled(context, ClearPackages.life.getVal());
        else
            return isPackageInstalled(context, ClearPackages.digitallife.getVal());
    }

    /**
     * Handles the callback from the key client after registration has happened.
     * @param success When true, the registration worked.
     */
    private void registrationCallbackHandler(Context context, String packageName, boolean success) {
        if (success) {
            Log.d(TAG, "Registration successful for DID " + getAppKeys(packageName).getDid());

            // Run all the additional callbacks specified by sub-classes.
            new Handler(context.getMainLooper()).post(() -> {
                List<Function<KeyClient, Void>> callbacks = registrationCallbacks.getOrDefault(packageName, null);
                if (callbacks != null) {
                    for (Function<KeyClient, Void> cb: callbacks) {
                        cb.apply(KeyClient.this);
                    }
                }

                Function<DidKeys, Void> keysCallback = packageKeyCallbacks.getOrDefault(packageName, null);
                if (keysCallback != null) {
                    Log.d(TAG, "Running callback from registration with appKeys for " + packageName);
                    keysCallback.apply(getAppKeys(packageName));
                }
            });
        } else {
            Log.w(TAG, "App key registration failed.");
            Function<DidKeys, Void> keysCallback = packageKeyCallbacks.getOrDefault(packageName, null);
            if (keysCallback != null) {
                keysCallback.apply(null);
            }
        }
    }

    public void setupKeys(Context context, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback, boolean isDemo) {
        setupKeys(context, context.getPackageName(), callback, notificationCallback, isDemo);
    }

    public void setupKeys(Context context, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback) {
        setupKeys(context, context.getPackageName(), callback, notificationCallback, false);
    }

    public void setupKeys(Context context, String packageName, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback) {
        setupKeys(context, packageName, callback, notificationCallback, false);
    }

    public void setupKeys(Context context, String packageName, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback, boolean isDemo) {
        if (!isClearLifeInstalled(context)) {
            Log.w(TAG, "ClearLIFE is not installed on the phone. No sense in setting up key client.");
            callback.apply(null);
            return;
        }

        synchronized (syncSetup) {
            DerivedKeyClient packageClient = getKeyClient(packageName);
            Boolean packageClientConnected = packageKeyClientConnected.getOrDefault(packageName, Boolean.FALSE);
            // We don't want problems caused by running this from multiple places at the same time.
            if (packageClient != null && packageClientConnected != null && packageClientConnected) {
                callback.apply(packageClient.getAppKeys());
                return;
            }

            // Concurrent hash maps can't handle null values...
            if (callback != null)
                packageKeyCallbacks.put(packageName, callback);
            if (notificationCallback != null)
                packageNotificationCallbacks.put(packageName, notificationCallback);

            packageClient = new DerivedKeyClient(context, this, packageName, isDemo);
            packageClient.create(context, 3000, kc -> {
                if (kc == null) {
                    Toast.makeText(context, context.getString(R.string.keysNotConnectedMsg), Toast.LENGTH_SHORT).show();
                    if (callback != null)
                        callback.apply(null);
                } else {
                    Log.d(TAG, String.format("Adding %s to concurrent map of package key clients for %s.", kc, packageName));
                    packageKeyClients.put(packageName, kc);
                    try {
                        kc.getAppKeys(packageName);
                    } catch (Exception e) {
                        // This happens if ClearLIFE has not been initialized yet.
                        Log.e(TAG, "No app keys created; probably ClearLIFE is not initialized", e);
                        if (callback != null)
                            callback.apply(null);
                        return null;
                    }

                    // Also register the app keys now so that third party services will work.
                    Log.d(TAG, "Key application is now registering the app keys for " + packageName);
                    if (!kc.registerAppKeys(packageName, s -> {
                        packageKeyClientConnected.put(packageName, true);
                        registrationCallbackHandler(context, packageName, s);
                        return null;
                    })) {
                        Log.e(TAG, "Error registering app keys with home server for " + packageName);
                        Toast.makeText(context, context.getString(R.string.appRegistrationErrMsg), Toast.LENGTH_SHORT).show();
                    }
                }
                return null;
            });
        }
    }

    /**
     * Returns one of the extra key clients for an arbitrary package. Note that only ClearLIFE
     * will have non-null custom package derived key clients. Other apps should expect unhandled
     * exceptions.
     * @return Null if it hasn't been initialized.
     */
    public DerivedKeyClient getKeyClient(String packageName) {
        Log.d(TAG, "Returning key client for specified application name " + packageName);
        DerivedKeyClient result = packageKeyClients.getOrDefault(packageName, null);
        Log.d(TAG, "Result of package-specific key-client lookup is " + result);
        return result;
    }

    public DerivedKeyClient getKeyClient(Context context) {
        Log.d(TAG, "Returning key client for default application name " + context.getPackageName());
        return getKeyClient(context.getPackageName());
    }


}

