package com.clearos.identity;

public class MqttDefaultTopicEvent {
    private String packageName;
    private String appDid;

    public MqttDefaultTopicEvent(String packageName, String appDid) {
        this.packageName = packageName;
        this.appDid = appDid;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getAppDid() {
        return appDid;
    }
}
