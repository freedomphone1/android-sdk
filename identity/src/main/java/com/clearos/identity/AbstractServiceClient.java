package com.clearos.identity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.function.Function;

public abstract class AbstractServiceClient<T extends android.os.IInterface> implements IAbstractService<T> {
    private ServiceConnection iConnection;
    private T iService;
    private Context appContext;
    private IRemoteErrorHandler handler;
    private String packageName;
    private AbstractServiceClient<T> mInstance;

    private String TAG = "AbstractServiceClient";

    public AbstractServiceClient(Context appContext, IRemoteErrorHandler handler) {
        this(appContext, handler, appContext.getPackageName());
    }

    public AbstractServiceClient(Context appContext, IRemoteErrorHandler handler, String packageName) {
        this.appContext = appContext;
        this.handler = handler;
        this.packageName = packageName;
        mInstance = this;
    }

    public AbstractServiceClient(Context appContext) {
        this(appContext, new LoggingRemoteErrorHandler());
    }

    public AbstractServiceClient(Context appContext, String packageName) {
        this(appContext, new LoggingRemoteErrorHandler(), packageName);
    }

    /**
     * Overrides the default tag for logging.
     */
    public void setTag(String tag) {
        TAG = tag;
    }

    public IRemoteErrorHandler h() {
        return handler;
    }

    public IRemoteErrorHandler getHandler() {
        return handler;
    }

    public Context getAppContext() {
        return appContext;
    }

    /**
     * Return an instance of the service binder to perform client actions.
     */
    public T s() {
        return iService;
    }

    /**
     * Binds the crypto keys service to the application.
     */
    public T create(Context appContext) {
        return create(appContext, 2000, null);
    }

    /**
     * Binds the crypto keys service to the application.
     */
    public T create(Context appContext, Function<AbstractServiceClient<T>, Void> callback) {
        return create(appContext, 2000, callback);
    }

    /**
     * Binds the service.
     * @param timeout Number of milliseconds to wait for service to come up before failing.
     * @param callback Function to call once the service is connected.
     */
    public T create(Context appContext, long timeout, Function<AbstractServiceClient<T>, Void> callback) {
        Log.d(TAG, "Creating iService connection from client.");

        iConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected; wiring up service interface.");
                iService = getServiceStubInterface(service);
                Log.d("ICryptoKeysClient", "Binding - Service connected");

                // Register this client callbacks.
                try {
                    registerCallbacks(packageName);
                    Log.d(TAG, "Service callbacks registered successfully.");
                }catch (RemoteException e) {
                    Log.e(TAG, "Error registering for service callbacks.");
                    handler.onRemoteError(e);
                    callback.apply(null);
                    return;
                }

                if (callback != null) {
                    callback.apply(mInstance);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                iService = null;
                Log.d("ICryptoKeysClient", "Binding - Service disconnected");
            }

            @Override
            public void onBindingDied(ComponentName name) {
                Log.d(TAG, "Binding died for " + name.flattenToString());
            }

            @Override
            public void onNullBinding(ComponentName name) {
                Log.d(TAG, "Null binding for " + name.flattenToString());
            }
        };

        if (iService == null) {
            Intent it = getServiceIntent();
            Log.d(TAG, "Binding service from client.");
            if (!appContext.bindService(it, iConnection, Service.BIND_AUTO_CREATE)) {
                Log.e(TAG, "Bind service call was not successful.");
                callback.apply(null);
            } else {
                // Give the service 1.5 seconds to connect before continuing without binding.
                new Handler(appContext.getMainLooper()).postDelayed(() -> {
                    if (iService == null) {
                        callback.apply(null);
                    }
                }, timeout);
            }
        }
        return iService;
    }

    /**
     * Unbinds the crypto derived keys service from this app.
     */
    public void destroy() {
        if (iService != null) {
            // Unregister this client for push notifications.
            try {
                unregisterCallbacks(iService, packageName);
            } catch (RemoteException e) {
                Log.e(TAG, "Error unregistering from service callbacks.");
                handler.onRemoteError(e);
            }
        }

        appContext.unbindService(iConnection);
    }
}
