package com.clearos.identity;

public interface IMqttTopicHandler {
    void onSubscribeTopic(MqttTopic topic);
    void onDefaultTopics(MqttDefaultTopicEvent defaultTopicEvent);
    void onUnsubscribeTopic(MqttUnsubscribeEvent topic);
    void onUnsubscribeDefaultTopics(MqttUnsubscribeEvent topics);
}
