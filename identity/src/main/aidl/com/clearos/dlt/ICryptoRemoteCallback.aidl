// ICryptoRemoteCallback.aidl
package com.clearos.dlt;

// Declare any non-default types here with import statements

oneway interface ICryptoRemoteCallback {
    oneway void onAppKeyRegistration(String packageName, String context, boolean success);
}
